package android.example.hitungluas;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;


public class MainActivity extends AppCompatActivity {

    private EditText edtAlas, edtTinggi;
    private Button btnHitung;
    private TextView txtLuas;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // kenalkan komponen-komponen yang ada di layout activity_main

        edtAlas = findViewById(R.id.edt_alas);
        edtTinggi = findViewById(R.id.edt_tinggi);
        btnHitung = findViewById(R.id.btn_hitung);
        txtLuas = findViewById(R.id.txt_luas);

        getSupportActionBar().setTitle("Luas Jajar Genjang");

        // berikan action button hitung untuk menghitung hasil nya

        btnHitung.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String alas, tinggi;
                alas = edtAlas.getText().toString();
                tinggi = edtTinggi.getText().toString();

                // TextUtils berfungsi supaya ketika EditText nya tidak terisi/kosong Maka
                // nanti akan muncul notifikasi "Error"

                if (TextUtils.isEmpty(alas)) {
                    edtAlas.setError("Error !");
                    edtTinggi.requestFocus();
                } else if (TextUtils.isEmpty(tinggi)) {
                    edtTinggi.setError("Error !");
                    edtTinggi.requestFocus();
                } else {

                    // Masukan Rumus untuk menghitung Alas dan juga Tinggi nya

                    double a = Double.parseDouble(alas);
                    double t = Double.parseDouble(tinggi);
                    double hasil = a * t;

                    // Kemudian Hasil di tampilkan di TextView

                    txtLuas.setText("Hasil Luas Jajar Genjang  : " + hasil);
                }
            }
        });
    }
}